// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"


void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();

}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);
}

void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > TimeToExplose)
		{
			//Explose
			Explose();

		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	//Init Grenade
	TimerEnabled = true;
}

void AProjectileDefault_Grenade::Explose()
{
	float ExploseBaseDamage = ProjectileSetting.ExploseBaseDamage;
	float ExploseMinimalDamage = ProjectileSetting.ExploseMinimalDamage;
	float ProjectileRadiusMaxDamage = ProjectileSetting.ProjectileRadiusMaxDamage;
	float ProjectileAllRadiusDamage = ProjectileSetting.ProjectileAllRadiusDamage;
	float ExploseDamageFalloff = ProjectileSetting.ExploseDamageFalloff;

	if (ExploseMinimalDamage >= ExploseBaseDamage)
	{
		ExploseMinimalDamage = ExploseBaseDamage;
	}

	if (ProjectileRadiusMaxDamage >= ProjectileAllRadiusDamage)
	{
		ProjectileRadiusMaxDamage = ProjectileAllRadiusDamage;
	}
	if (ExploseDamageFalloff <= 0)
	{
		ExploseDamageFalloff = 0.1f;
	}

	TimerEnabled = false;
	if (ProjectileSetting.ExploseFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExploseFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if (ProjectileSetting.ExploseSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExploseSound, GetActorLocation());
	}

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ExploseBaseDamage,
		ExploseMinimalDamage,
		GetActorLocation(),
		ProjectileRadiusMaxDamage,
		ProjectileAllRadiusDamage,
		ExploseDamageFalloff,
		NULL, IgnoredActor, this, nullptr);

	if (ShowDebugSphere)
	{
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileRadiusMaxDamage, 10, FColor::Red, true, 5, 0, 2);
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileAllRadiusDamage, 10, FColor::Blue, true, 5, 0, 2);
	}

	this->Destroy();
}

