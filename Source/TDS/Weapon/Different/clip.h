// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "clip.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API AClip : public AActor
{
	GENERATED_BODY()

public:
	AClip();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* ClipMeshComponent = nullptr;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	void InitClip(UStaticMesh* InitParam);

};
