// Fill out your copyright notice in the Description page of Project Settings.


#include "cartridge.h"


ACartridge::ACartridge() 
{
	CartridgeMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	CartridgeMeshComponent->SetupAttachment(RootComponent);
	CartridgeMeshComponent->SetCanEverAffectNavigation(false);
}

void ACartridge::BeginPlay()
{
}

void ACartridge::InitCartridge(UStaticMesh* InitParam, FVector value)
{
	CartridgeMeshComponent->SetStaticMesh(InitParam);
	CartridgeMeshComponent->AddImpulse(force*value*CartridgeMeshComponent->GetMass());
}
