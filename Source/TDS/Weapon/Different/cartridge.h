// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "cartridge.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API ACartridge : public AActor
{
	GENERATED_BODY()

public:
	ACartridge();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* CartridgeMeshComponent = nullptr;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	void InitCartridge(UStaticMesh* InitParam, FVector value);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cartrige")
	float force = 200;

};
