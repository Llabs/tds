// Fill out your copyright notice in the Description page of Project Settings.


#include "clip.h"

AClip::AClip()
{
	ClipMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ClipMesh"));
	ClipMeshComponent->SetupAttachment(RootComponent);
	ClipMeshComponent->SetCanEverAffectNavigation(false);
}

void AClip::BeginPlay()
{
}

void AClip::InitClip(UStaticMesh * InitParam)
{
	ClipMeshComponent->SetStaticMesh(InitParam);
}
