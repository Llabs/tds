// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDS/FuncLibrary/TDS_IGameActor.h"
#include "TDS/Weapon/TDS_Effect.h"
#include "EnvironmentStructure.generated.h"

UCLASS()
class TDS_API AEnvironmentStructure : public AActor, public ITDS_IGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnvironmentStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	EPhysicalSurface GetSurfuceType() override;

	TArray<UTDS_Effect*> GetAllCurrentEffects() override;
	void RemoveEffect(UTDS_Effect* RemoveEffect)override;
	void AddEffect(UTDS_Effect* newEffect)override;

	//Effect
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		TArray<UTDS_Effect*> Effects;
	//Interface
	
	void ParticleSpavnPoint_Implementation(FName& BoneName, FVector& Offset)override;
	
};
